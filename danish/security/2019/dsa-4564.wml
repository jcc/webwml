#use wml::debian::translation-check translation="0cc922cd661d77cfeed7f482bce1cfba75c197ae" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12207">CVE-2018-12207</a>

    <p>Man opdagede at på Intel-CPU'er, der undersøtter hardwarevirtualisering 
    med Extended Page Tables (EPT), kunne en gæste-VM manipulere 
    hukommelseshåndteringshardware medførende en Machine Check Error (MCE) og 
    lammelsesangreb (hængende system eller nedbrud).</p>

    <p>Gæsten kunne udløse fejlen ved at ændre sidetabeller uden en TLB-flush, 
    således at både 4 KB- og 2 MB-registreringer på den samme virtuelle adresse, 
    blev indlæst i instruktion-TLB'en (iTLB).  Denne opdatering implementerer en 
    afhjælpelse i KVM, som forhindrer gæste-VM'er i at indlæse 2 
    MB-registreringer i iTLB'en.  Det reducerer gæste-VM'ers ydeevne.</p>

    <p>Yderligere oplysninger om afhjælpelsen, finder man på
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/multihit.html">
    eller i pakkerne linux-doc-4.9 eller linux-doc-4.19.</p>

    <p>En qemu-opdatering, som tilføjer understøttelse af funktionaliten 
    PSCHANGE_MC_NO feature, som gør det muligt at deaktivere iTLB 
    Multihit-afhjælpelser i indlejrede hypervisorer, gøres tilgængelig via DSA 
    4566-1.</p>

    <p>Intels redegørelse for problemet finder man på
    <url "https://software.intel.com/security-software-guidance/insights/deep-dive-machine-check-error-avoidance-page-size-change-0">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0154">CVE-2019-0154</a>

    <p>Intel opdagede at i deres 8. og 9. generation-CPU'er, kunne læsning af 
    visse registre, mens GPU'en er i lavt strømforbrug-tilstand, medføre at 
    systemet hænger.  En lokal bruger, med rettigheder til at anvende GPU'en, 
    kunne udnytte fejlen til lammelsesangreb.</p>

    <p>Denne opdatering afhjælper problemet ved hjælp af ændringer i 
    i915-driveren.</p>

    <p>De påvirkede chips (gen8 og gen9) er opremset på
    <url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0155">CVE-2019-0155</a>

    <p>Intel opdagede at i deres 9. generation- og nyere CPU'er, manglende der 
    et sikkerhedstjek i Blitter Command Streamer (BCS).  En lokal bruger med 
    rettigheder til at anvende GPU'en, kunne anvende fejlen til at tilgå enhver 
    hukommelsesadresse, som GPU'en har adgang til, hvilket kunne medføre 
    lammelsesangreb (hukommelseskorruption eller nedbrud), lækage af følsomme 
    oplysninger eller rettighedsforøgelse.</p>

    <p>Denne opdatering afhjælper problemet ved at tilføje sikkerhedstjekket i 
    i915-driveren.</p>

    <p>De påvirkede chips (gen9 og senere) er opremset på
    <url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen9">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11135">CVE-2019-11135</a>

    <p>Man opdagede at i Intel-CPU'er, der understøtter transaktionshukommelse 
    (TSX), kunne en transaktion, som vil blive afbrudt, fortsætte med at at 
    blive udført spekulativt, læsende følsomme data fra interne buffere og lække 
    dem gennem afhængige handlinger.  Intel kalder det <q>TSX Asynchronous 
    Abort</q> (TAA).</p>

    <p>CPU'er påvirket af det tidligere offentliggjorte Microarchitectural Data 
    Sampling-problemer (MDS)
    (<a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>),
    løser eksisterende hjælpelser også dette problem.</p>

    <p>For processorer, der er sårbare over for TAA, men ikke MDS, deaktiveres 
    TSX som standard med denne opdatering.  Afhjælpelsen kræver opdateret 
    CPU-mikrokode.  En opdateret pakke med intel-microcode (kun tilgængelig i 
    Debian-non-free) vil blive stillet til rådighed via DSA 4565-1.  Den 
    opdaterede CPU-mikrokode kan også være tilgængelig som en del af en 
    opdatering af systemets firmware (<q>BIOS</q>).</p>

    <p>Yderligere oplysninger om hjælpelsen finder man på
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/tsx_async_abort.html">
    eller i pakkerne linux-doc-4.9 eller linux-doc-4.19.</p>

    <p>Intels redegørelse for problemet finder man på
    <url "https://software.intel.com/security-software-guidance/insights/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort">.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 4.9.189-3+deb9u2.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 4.19.67-2+deb10u2.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4564.data"
