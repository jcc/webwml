<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that the Berkeley DB reads DB_CONFIG from the current
working directory, leading to information leak by tricking privileged
processes into reading arbitrary files.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.1.29-5+deb7u1.</p>

<p>We recommend that you upgrade your db packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1135.data"
# $Id: $
