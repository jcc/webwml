<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox web
browser: Multiple memory safety errors, use-after-frees and other
implementation errors may lead to the execution of arbitrary code or
information leaks or privilege escalation.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
45.7.0esr-1~deb7u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-800.data"
# $Id: $
