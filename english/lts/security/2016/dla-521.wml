<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox web
browser: Multiple memory safety errors, buffer overflows and other
implementation errors may lead to the execution of arbitrary code or
spoofing.</p>

<p>Wait, Firefox? No more references to Iceweasel? That's right, Debian no
longer applies a custom branding. Please see these links for further
information:</p>
<ul>
<li><a href="https://glandium.org/blog/?p622">https://glandium.org/blog/?p622</a></li>
<li><a href="https://en.wikipedia.org/wiki/Mozilla_software_rebranded_by_Debian">https://en.wikipedia.org/wiki/Mozilla_software_rebranded_by_Debian</a></li>
</ul>

<p>Debian follows the extended support releases (ESR) of Firefox. Support
for the 38.x series has ended, so starting with this update we're now
following the 45.x releases and this update to the next ESR is also the
point where we reapply the original branding.</p>

<p>Transition packages for the iceweasel packages are provided which
automatically upgrade to the new version. Since new binary packages need
to be installed, make sure to allow that in your upgrade procedure (e.g.
by using "apt-get dist-upgrade" instead of "apt-get upgrade").</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
45.2.0esr-1~deb7u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-521.data"
# $Id: $
