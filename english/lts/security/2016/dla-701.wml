<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilites have been found in memcached, a high-performance
memory object caching system. A remote attacker could take advantage of
these flaws to cause a denial of service (daemon crash), or potentially
to execute arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-7291">CVE-2013-7291</a>

    <p>It was discovered that memcached, when running in verbose mode, can
    be crashed by sending carefully crafted requests that trigger an
    unbounded key print, resulting in a daemon crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8704">CVE-2016-8704</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-8705">CVE-2016-8705</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-8706">CVE-2016-8706</a>

    <p>Aleksandar Nikolic of Cisco Talos found several vulnerabilities in
    memcached. A remote attacker could cause an integer overflow by
    sending carefully crafted requests to the memcached server,
    resulting in a daemon crash.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4.13-0.2+deb7u2.</p>

<p>We recommend that you upgrade your memcached packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-701.data"
# $Id: $
