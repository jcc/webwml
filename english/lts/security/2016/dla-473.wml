<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was found in how hostapd and wpa_supplicant writes the
configuration file update for the WPA/WPA2 passphrase parameter. If
this parameter has been updated to include control characters either
through a WPS operation (<a href="https://security-tracker.debian.org/tracker/CVE-2016-4476">CVE-2016-4476</a>) 
or through local configuration
change over the wpa_supplicant control interface (<a href="https://security-tracker.debian.org/tracker/CVE-2016-4477">CVE-2016-4477</a>), the
resulting configuration file may prevent the hostapd and
wpa_supplicant from starting when the updated file is used. In
addition for wpa_supplicant, it may be possible to load a local
library file and execute code from there with the same privileges
under which the wpa_supplicant process runs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4476">CVE-2016-4476</a>

    <p>hostapd 0.6.7 through 2.5 and wpa_supplicant 0.6.7 through 2.5 do
    not reject \n and \r characters in passphrase parameters, which
    allows remote attackers to cause a denial of service (daemon
    outage) via a crafted WPS operation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4477">CVE-2016-4477</a>

    <p>wpa_supplicant 0.4.0 through 2.5 does not reject \n and \r
    characters in passphrase parameters, which allows local users to
    trigger arbitrary library loading and consequently gain privileges,
    or cause a denial of service (daemon outage), via a crafted (1)
    SET, (2) SET_CRED, or (3) SET_NETWORK command.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0-3+deb7u4.</p>

<p>We recommend that you upgrade your wpa packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-473.data"
# $Id: $
