msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr ""

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr ""

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr ""

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr ""

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr ""

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid "In the following list, <q>current</q> is used for positions that are\ntransitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:105
msgid "Distribution"
msgstr ""

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:237
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:240
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:244
msgid "Publicity team"
msgstr ""

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:315
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:343
msgid "Support and Infrastructure"
msgstr ""

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr ""

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr ""

#: ../../english/intro/organization.data:100
msgid "Secretary"
msgstr ""

#: ../../english/intro/organization.data:108
msgid "Development Projects"
msgstr ""

#: ../../english/intro/organization.data:109
msgid "FTP Archives"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:117
msgid "FTP Assistants"
msgstr ""

#: ../../english/intro/organization.data:123
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:127
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:129
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:133
msgid "Release Management"
msgstr ""

#: ../../english/intro/organization.data:135
msgid "Release Team"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Quality Assurance"
msgstr ""

#: ../../english/intro/organization.data:145
msgid "Installation System Team"
msgstr ""

#: ../../english/intro/organization.data:146
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:147
msgid "Release Notes"
msgstr ""

#: ../../english/intro/organization.data:149
msgid "CD/DVD/USB Images"
msgstr ""

#: ../../english/intro/organization.data:151
msgid "Production"
msgstr ""

#: ../../english/intro/organization.data:158
msgid "Testing"
msgstr ""

#: ../../english/intro/organization.data:160
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:164
msgid "Autobuilding infrastructure"
msgstr ""

#: ../../english/intro/organization.data:166
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:173
msgid "Buildd administration"
msgstr ""

#: ../../english/intro/organization.data:191
msgid "Documentation"
msgstr ""

#: ../../english/intro/organization.data:196
msgid "Work-Needing and Prospective Packages list"
msgstr ""

#: ../../english/intro/organization.data:198
msgid "Ports"
msgstr ""

#: ../../english/intro/organization.data:228
msgid "Special Configurations"
msgstr ""

#: ../../english/intro/organization.data:230
msgid "Laptops"
msgstr ""

#: ../../english/intro/organization.data:231
msgid "Firewalls"
msgstr ""

#: ../../english/intro/organization.data:232
msgid "Embedded systems"
msgstr ""

#: ../../english/intro/organization.data:247
msgid "Press Contact"
msgstr ""

#: ../../english/intro/organization.data:249
msgid "Web Pages"
msgstr ""

#: ../../english/intro/organization.data:261
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:266
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:271
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:279
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:286
msgid "Events"
msgstr ""

#: ../../english/intro/organization.data:293
msgid "DebConf Committee"
msgstr ""

#: ../../english/intro/organization.data:300
msgid "Partner Program"
msgstr ""

#: ../../english/intro/organization.data:305
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:327
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:329
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:330
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:333
msgid "OASIS: Organization\n      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:336
msgid "OVAL: Open Vulnerability\n      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:339
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:346
msgid "User support"
msgstr ""

#: ../../english/intro/organization.data:413
msgid "Bug Tracking System"
msgstr ""

#: ../../english/intro/organization.data:418
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""

#: ../../english/intro/organization.data:427
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:433
msgid "Debian Account Managers"
msgstr ""

#: ../../english/intro/organization.data:437
msgid "To send a private message to all DAMs, use the GPG key 57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:438
msgid "Keyring Maintainers (PGP and GPG)"
msgstr ""

#: ../../english/intro/organization.data:442
msgid "Security Team"
msgstr ""

#: ../../english/intro/organization.data:453
msgid "Consultants Page"
msgstr ""

#: ../../english/intro/organization.data:458
msgid "CD Vendors Page"
msgstr ""

#: ../../english/intro/organization.data:461
msgid "Policy"
msgstr ""

#: ../../english/intro/organization.data:464
msgid "System Administration"
msgstr ""

#: ../../english/intro/organization.data:465
msgid "This is the address to use when encountering problems on one of Debian's machines, including password problems or you need a package installed."
msgstr ""

#: ../../english/intro/organization.data:475
msgid "If you have hardware problems with Debian machines, please see <a href=\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should contain per-machine administrator information."
msgstr ""

#: ../../english/intro/organization.data:476
msgid "LDAP Developer Directory Administrator"
msgstr ""

#: ../../english/intro/organization.data:477
msgid "Mirrors"
msgstr ""

#: ../../english/intro/organization.data:484
msgid "DNS Maintainer"
msgstr ""

#: ../../english/intro/organization.data:485
msgid "Package Tracking System"
msgstr ""

#: ../../english/intro/organization.data:487
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:494
msgid "<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:497
msgid "Salsa administrators"
msgstr ""

