msgid ""
msgstr ""
"Project-Id-Version: organization.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-12-22 02:31+0900\n"
"Last-Translator: Nobuhiro Iwamatsu <iwamatsu@debian.org>\n"
"Language-Team: Japanese <debian-www@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "委任のメール"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "指名のメール"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "委任"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "委任"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "現職"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "構成員"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "マネージャー"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "安定版リリース管理者"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "ウィザード"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
#, fuzzy
msgid "chair"
msgstr "委員長"

#: ../../english/intro/organization.data:41
msgid "assistant"
msgstr "補佐"

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "書記"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "執行部"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "ディストリビューション"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
msgid "Publicity team"
msgstr "広報チーム"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "サポートと設備"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "リーダー"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "技術委員会"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "書記"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "開発プロジェクト"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "FTP アーカイブ"

#: ../../english/intro/organization.data:105
msgid "FTP Masters"
msgstr "FTP マスター"

#: ../../english/intro/organization.data:111
msgid "FTP Assistants"
msgstr "FTP アシスタント"

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr "FTP ウィザード"

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr "Backports チーム"

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "リリース管理"

#: ../../english/intro/organization.data:128
msgid "Release Team"
msgstr "リリースチーム"

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "品質保証"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "インストールシステムチーム"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Release Notes"
msgstr "リリースノート"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "CD イメージ"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "製品"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "テスト"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:162
msgid "Autobuilding infrastructure"
msgstr "オートビルドインフラ"

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr "ビルド要求チーム"

#: ../../english/intro/organization.data:171
msgid "Buildd administration"
msgstr "ビルドデーモン管理"

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "ドキュメンテーション"

#: ../../english/intro/organization.data:194
msgid "Work-Needing and Prospective Packages list"
msgstr "作業の必要なパッケージ一覧"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "移植"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "特殊な設定"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "ラップトップ"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "ファイアウォール"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr "組み込みシステム"

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "広報窓口"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "ウェブページ"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr "教育"

#: ../../english/intro/organization.data:269
msgid "Debian Women Project"
msgstr "Debian Women プロジェクト"

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr "嫌がらせ反対"

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "イベント"

#: ../../english/intro/organization.data:289
#, fuzzy
msgid "DebConf Committee"
msgstr "技術委員会"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "パートナープログラム"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr "ハードウェア寄付コーディネーション"

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "ユーザサポート"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "バグ追跡システム (BTS)"

#: ../../english/intro/organization.data:414
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "メーリングリスト管理とメーリングリストアーカイブ"

#: ../../english/intro/organization.data:422
msgid "New Members Front Desk"
msgstr "新規メンバー受付"

#: ../../english/intro/organization.data:428
msgid "Debian Account Managers"
msgstr "Debian アカウント管理者"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"(全) DAM に私信のメールを送る際には 57731224A9762EA155AB2A530CA8D15BB24D96F2 "
"の GPG 鍵を使ってください。"

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "公開鍵管理者 (PGP および GPG)"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "セキュリティチーム"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "コンサルタントページ"

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr "CD ベンダページ"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "ポリシー"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "システム管理"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"こちらの電子メールアドレスは、パスワードに関する問題や、新たなパッケージイン"
"ストールの依頼など、Debianマシンのいずれかに何か問題のある際に用いる連絡先で"
"す。"

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Debian マシンに問題があれば、<a href=\"https://db.debian.org/machines.cgi"
"\">Debian マシン</a>のページを参照してください。そこにはマシンごとの管理情報"
"があります。"

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP 開発者ディレクトリ管理者"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "ミラー"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "DNS 管理"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "パッケージ追跡システム"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:488
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr "<a name=\"trademark\" href=\"m4_HOME/trademark\">商標</a>利用申請"

#: ../../english/intro/organization.data:491
#, fuzzy
msgid "Salsa administrators"
msgstr "Alioth 管理者"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Individual Packages"
#~ msgstr "個々のパッケージ"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "1歳から99歳までのこどものための Debian"

#~ msgid "Debian for medical practice and research"
#~ msgstr "臨床医学と医学研究のための Debian"

#~ msgid "Debian for education"
#~ msgstr "教育のための Debian"

#~ msgid "Debian in legal offices"
#~ msgstr "法律事務所のための Debian"

#~ msgid "Debian for people with disabilities"
#~ msgstr "障害者のための Debian"

#~ msgid "Debian for science and related research"
#~ msgstr "科学研究のための Debian"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "教育のための Debian"

#~ msgid "Live System Team"
#~ msgstr "ライブシステムチーム"

#~ msgid "Auditor"
#~ msgstr "監事"

#~ msgid "Publicity"
#~ msgstr "広報"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian メンテナ (DM) 公開鍵管理者"

#~ msgid "DebConf chairs"
#~ msgstr "DebConf 役員"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth 管理者"
