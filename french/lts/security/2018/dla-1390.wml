#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le Qualys Research Labs a découvert plusieurs vulnérabilités dans procps, un
ensemble d’utilitaires en ligne de commande et en mode plein écran pour
parcourir procfs. Le projet « Common vulnérabilités et Exposures » (CVE)
identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1122">CVE-2018-1122</a>

<p>top lisait sa configuration à partir du répertoire de travail courant si
aucun $HOME n'était configuré. Si top était lancé à partir d'un répertoire
accessible en écriture par l'attaquant (tel que /tmp), cela pouvait avoir
pour conséquence une augmentation de droits locale.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1123">CVE-2018-1123</a>

<p>Un déni de service à l'encontre de l'invocation de ps par un autre
utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1124">CVE-2018-1124</a>

<p>Un dépassement d'entier dans la fonction file2strvec() de libprocps
pourrait avoir pour conséquence une augmentation de droits locale.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1125">CVE-2018-1125</a>

<p>Un dépassement de pile dans pgrep pourrait avoir pour conséquence un
déni de service pour un utilisateur se servant de pgrep pour inspecter un
processus contrefait pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1126">CVE-2018-1126</a>

<p>Des paramètres incorrects de taille d'entier utilisés dans des
enveloppes pour des allocateurs C standard pourraient provoquer une
troncature d'entier et mener à des problèmes de dépassement d'entier.</p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:3.3.3.3+deb7u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets procps.</p>

<p>L’équipe LTS de Debian souhaite remercier Abhijith PA pour la préparation de
cette mise à jour.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1390.data"
# $Id: $
