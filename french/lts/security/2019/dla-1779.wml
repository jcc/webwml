#use wml::debian::translation-check translation="f9dd58eba0254b587e5b136b4126b83065f33b28" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Dans 389-ds-base jusqu’à la version 1.4.1.2, les requêtes étaient gérées par
des processus légers « worker ». Chaque socket était attendu par le worker pour
au plus <q>ioblocktimeout</q> secondes. Cependant, ce délai s’appliquait
seulement aux requêtes non chiffrées. Les connexions utilisant SSL/TLS ne
tenaient pas compte de ce délai lors de lectures et pouvaient être suspendues
plus longtemps. Un attaquant non authentifié pouvait créer de manière répétée
des requêtes LDAP en attente pour planter tous les workers, aboutissant à
un déni de service.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.3.3.5-4+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets 389-ds-base.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1779.data"
# $Id: $
