#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui pourraient conduire à une élévation des privilèges, un déni de service ou une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2012-6704">CVE-2012-6704</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9793">CVE-2016-9793</a>

<p>Eric Dumazet a trouvé qu'un utilisateur local avec la capacité CAP_NET_ADMIN
pourrait fixer la taille du tampon d'une socket à une valeur négative, conduisant à
un déni de service ou à un autre impact de sécurité. De plus, dans
les versions du noyau antérieures à 3.5, n'importe quel utilisateur pouvait faire cela si sysctl
net.core.rmem_max était changé à une valeur très grande.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1350">CVE-2015-1350</a> / #770492

<p>Ben Harris a signalé que des utilisateurs locaux pourraient supprimer l'attribut
set-capability de n'importe quel fichier visible par eux, permettant un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8962">CVE-2015-8962</a>

<p>Calvin Owens a trouvé que la suppression d'un appareil SCSI alors qu'il était en train
d'être consulté à l'aide d'un driver SCSI générique (sg) conduisait à une double libération de zone de mémoire,
causant éventuellement un déni de service (plantage ou corruption de mémoire) 
ou une élévation des privilèges. Cela pourrait être exploité par
des utilisateurs locaux avec la permission d'accéder au nœud d'un appareil SCSI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8963">CVE-2015-8963</a>

<p>Sasha Levin a signalé que la déconnexion à chaud d'un CPU conduisait à une
utilisation de mémoire après libération par le sous-système d'évènements de performance (perf),
causant éventuellement un déni de service (plantage ou corruption de mémoire)
ou une élévation des privilèges. Cela pourrait être exploité par n'importe quel utilisateur local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8964">CVE-2015-8964</a>

<p>Il a été trouvé que le sous-système terminal/serial (tty) ne
réinitialisait pas de façon fiable l'état du tampon du terminal lorsque la discipline de ligne
du terminal était changée. Cela pourrait permettre à un utilisateur local avec un accès
à un terminal de lire des informations sensibles depuis la mémoire
du noyau.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7097">CVE-2016-7097</a>

<p>Jan Kara a trouvé que changer l'ACL POSIX d'un fichier n'effaçait jamais
son drapeau set-group-ID, ce qui devrait être fait si l'utilisateur qui le change
n'est pas un membre du group-owner. Dans certains cas, cela
permettrait au user-owner d'un exécutable d'accéder aux privilèges
du group-owner.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7910">CVE-2016-7910</a>

<p>Vegard Nossum a découvert qu'un échec d'allocation mémoire lors
d'une lecture de /proc/diskstats ou /proc/partitions pourrait conduire à
une utilisation de mémoire après libération, causant éventuellement un déni de service (plantage
or corruption de mémoire) ou une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7911">CVE-2016-7911</a>

<p>Dmitry Vyukov a signalé qu'une situation de compétition entre les appels systèmes ioprio_get() et
ioprio_set() pourrait aboutir à une utilisation de mémoire après libération,
causant éventuellement un déni de service (plantage) ou une fuite d'informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7915">CVE-2016-7915</a>

<p>Benjamin Tissoires a trouvé que les appareils HID pourraient déclencher un accès en mémoire hors limites 
de la mémoire dans le cœur HID. Un utilisateur physiquement présent
pourrait éventuellement utiliser cela pour un déni de service (plantage) ou une fuite
d'informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8399">CVE-2016-8399</a>

<p>Qidan He a signalé que l'implémentation de ping pour socket IPv4 ne
validait pas la longueur des paquets à envoyer. Un utilisateur avec
la permission d'utiliser des sockets pour faire des pings pourrait causer une lecture hors limites,
aboutissant éventuellement à un déni de service ou à une fuite d'informations.
Cependant, sur les systèmes Debian aucun utilisateur n'a la droit de créer des sockets
pour faire des pings par défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8633">CVE-2016-8633</a>

<p>Eyal Itkin a signalé que le pilote IP-over-Firewire
(firewire-net) ne validait pas le décalage ou la longueur dans les en-têtes
de fragmentation link-layer. Cela permettait à des systèmes distants connectés par
Firewire d'écrire en mémoire après un tampon de paquet, conduisant à un
déni de service (plantage) ou à une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8645">CVE-2016-8645</a>

<p>Marco Grassi a signalé que si un filtre de socket (BPF program)
attaché à une socket TCP tronquait ou supprimait les en-têtes TCP, cela
pourrait causer un déni de service (plantage). Cela était exploitable par
n'importe quel utilisateur local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8655">CVE-2016-8655</a>

<p>Philip Pettersson a trouvé que l'implémentation sockets de paquet
(famille AF_PACKET) avait une situation de compétition entre activer un
tampon circulaire de transmission et changer la version des tampons utilisés,
qui pourrait aboutir à utilisation de mémoire après libération. Un utilisateur local avec la
capacité CAP_NET_ADMIN pourrait utiliser cela pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9178">CVE-2016-9178</a>

<p>Al Viro a trouvé qu'un échec de lecture depuis la mémoire utilisateur pouvait
aboutir à une fuite d'informations sur l'architecture x86 (amd64 ou i386).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9555">CVE-2016-9555</a>

<p>Andrey Konovalov a signalé que l'implémentation de SCTP ne
validait pas <q>à l'improviste</q> la longueur des morceaux de paquet assez tôt. Un
système distant pourrait utiliser cela pour provoquer un déni de service
(plantage) ou un autre impact de sécurité pour des systèmes utilisant SCTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9576">CVE-2016-9576</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-10088">CVE-2016-10088</a>

<p>Dmitry Vyukov a signalé que l'utilisation de splice() avec le driver générique
SCSI aboutissait à une corruption de la mémoire du noyau. Des utilisateurs locaux avec
la permission d'accéder à un nœud d'appareil SCSI pourrait exploiter cela pour
une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9756">CVE-2016-9756</a>

<p>Dmitry Vyukov a signalé que KVM pour l'architecture x86 (amd64 ou
i386) ne gérait pas correctement l'échec de certaines instructions
qui nécessitent une émulation logicielle sur des processeurs plus anciens. Cela pourrait
être exploité par des systèmes invités pour divulguer des informations sensibles ou pour
un déni de service (log spam).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9794">CVE-2016-9794</a>

<p>Baozeng Ding a signalé une situation de compétition dans le sous-système ALSA (audio)
qui pourrait aboutir à une utilisation de mémoire après libération. Des utilisateurs locaux avec
un accès à un appareil audio PCM pourraient exploiter cela pour un déni de
service (plantage ou corruption de mémoire) ou un autre impact de sécurité.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.2.84-1. Cette version inclut aussi des corrections de bogue depuis
version 3.2.84  de l’amont et met à jour l’ensemble de fonctionnalités
 PREEMPT_RT à la version 3.2.84-rt122. Finalement, cette version ajoute l’option
pour atténuer les problèmes de sécurité dans le sous-système d’évènements de
performance (perf) en désactivant les utilisateurs non privilégiés. Cela peut
être fait en réglant kernel.perf_event_paranoid=3.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.39-1 qui sera incluse dans la prochaine publication
intermédiaire (8.6).</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-772.data"
# $Id: $
