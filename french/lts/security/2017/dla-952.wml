#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans kde4libs, les bibliothèques
centrales pour toutes les applications de KDE 4.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6410">CVE-2017-6410</a>

<p>Itzik Kotler, Yonatan Fridburg et Amit Klein de Safebreach Labs
ont signalé que les URL ne sont pas vérifiées avant de les passer à
FindProxyForURL, permettant éventuellement à un attaquant distant d'obtenir
des informations sensibles à l'aide d'un fichier PAC contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8422">CVE-2017-8422</a>

<p>Sebastian Krahmer de SUSE a découvert que le cadriciel KAuth renfermait
un défaut logique dans lequel le service invoquant dbus n'est pas
correctement vérifié. Ce défaut permet d'usurper l'identité de l'appelant
et d'obtenir les droits du superutilisateur à partir d'un compte non
privilégié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-2074">CVE-2013-2074</a>

<p>Il a été découvert que KIO afficherait des identifiants d’authentification
dans quelques cas d’erreur.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4:4.8.4-4+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets kde4libs.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-952.data"
# $Id: $
