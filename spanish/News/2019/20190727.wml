#use wml::debian::translation-check translation="b061a3eb5a284dce5a149a8505b3121bd7e4c6b6"
<define-tag pagetitle>Clausura de DebConf19 en Curitiba y anuncio de fechas para DebConf20</define-tag>

<define-tag release_date>2019-07-27</define-tag>
#use wml::debian::news


<p>
Hoy sábado, 27 de julio de 2019, se ha clausurado la conferencia anual de
desarrolladores y contribuidores de Debian.
Con más de 380 asistentes de 50 países y más de
145 eventos entre charlas, debates, 
reuniones informales (BoF, por sus siglas en inglés: «Birds of a Feather»), talleres y otras actividades,
<a href="https://debconf19.debconf.org">DebConf19</a> ha sido un gran éxito.
</p>

<p>
La conferencia estuvo precedida, del 14 al 19 de julio, por el DebCamp anual,
que se centró, por un lado, en trabajo individual y esprints de equipos para colaboración presencial
relacionada con el desarrollo de Debian y, por otro, en la celebración de un taller de tres días sobre empaquetado,
en el que nuevos contribuidores y contribuidoras pudieron iniciarse en el empaquetado de Debian.
</p>

<p>
La <a href="https://debconf19.debconf.org/news/2019-07-20-open-day/">jornada de puertas abiertas</a>,
celebrada el 20 de julio con más de 250 asistentes, contó con presentaciones
y talleres de interés para una audiencia más amplia, una feria de empleo con stands de
varios de los patrocinadores de la DebConf19 y una fiesta de instalación de Debian.
</p>

<p>
La conferencia de desarrolladores de Debian propiamente dicha comenzó el sábado, 21 de julio de 2019.
Junto a sesiones plenarias como los tradicionales «bits» del líder del proyecto Debian,
charlas relámpago, demos en vivo
y el anuncio de la DebConf del próximo año
(<a href="https://wiki.debian.org/DebConf/20">DebConf20</a> en Haifa, Israel), 
hubo varias sesiones relacionadas con la reciente publicación de Debian 10 buster
y alguna de sus características, así como actualizaciones sobre varios proyectos y
equipos internos de Debian, sesiones de debate informales (BoF) de los equipos de lenguajes de programación,
de adaptaciones («ports»), de infraestructura y de comunidad
y muchos otros actos de interés acerca de Debian y del software libre.
</p>

<p>
La <a href="https://debconf19.debconf.org/schedule/">programación</a>
se actualizaba a diario con actividades ad-hoc planificadas por asistentes
a la conferencia durante el desarrollo de la misma.
</p>

<p>
Para quienes no pudieron asistir, la mayoría de las charlas y sesiones se
grabaron y se retransmitieron en directo, y los vídeos están
disponibles en el
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/">archivo web de encuentros Debian</a>.
Casi todas las sesiones permitieron la participación remota a través de aplicaciones de mensajería
IRC o de documentos de texto colaborativos.
</p>

<p>
El <a href="https://debconf19.debconf.org/">sitio web DebConf19</a>
permanecerá activo como archivo y continuará ofreciendo
enlaces a las presentaciones y vídeos de charlas y eventos.
</p>

<p>
El próximo año, <a href="https://wiki.debian.org/DebConf/20">DebConf20</a> tendrá lugar en Haifa, Israel,
entre el 23 y el 29 de agosto de 2020. 
Siguiendo la tradición, antes de la DebConf los organizadores y organizadoras locales de Israel 
darán comienzo a las actividades de la conferencia con un DebCamp (del 16 al 22 de agosto)
centrado en el trabajo individual y de equipo para la mejora de la
distribución.
</p>

<p>
DebConf está comprometido con el establecimiento de un ambiente seguro y acogedor para todos los participantes.
Durante la conferencia están disponibles varios equipos (Recepción, Equipo de bienvenida y Equipo
antiacoso) para ayudar a que tanto quienes participan presencialmente como quienes lo hacen de forma remota vivan la mejor experiencia
durante la conferencia y para ayudar a encontrar soluciones a cualquier problema que pueda surgir.
Consulte la <a href="https://debconf19.debconf.org/about/coc/">página sobre el código de conducta en el sitio web de la DebConf19</a>
para más detalles sobre esta cuestión.
</p>

<p>
Debian agradece el compromiso de numerosos <a href="https://debconf19.debconf.org/sponsors/">patrocinadores</a>
por su apoyo a la DebConf19, especialmente el de nuestros patrocinadores platino:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
y <a href="https://www.lenovo.com">Lenovo</a>.
</p>

<h2>Acerca de Debian</h2>

<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser
un proyecto comunitario verdaderamente libre. Desde entonces el proyecto
ha crecido hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios de todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido a 70 idiomas y soporta
una gran cantidad de arquitecturas de ordenadores, por lo que el proyecto
se refiere a sí mismo como <q>el sistema operativo universal</q>.
</p>

<h2>Acerca de DebConf</h2>

<p>
DebConf es la conferencia de desarrolladores del proyecto Debian. Además de un
amplio programa de charlas técnicas, sociales y sobre reglamentación, DebConf proporciona una
oportunidad para que desarrolladores, contribuidores y otras personas interesadas se
conozcan en persona y colaboren más estrechamente. Se ha celebrado
anualmente desde 2000 en lugares tan diversos como Escocia, Argentina y
Bosnia-Herzegovina. Hay más información sobre DebConf disponible en
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Acerca de Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> es la mayor empresa de alojamiento web de Suiza.
También ofrece servicios de respaldo y de almacenamiento, soluciones para organizadores de eventos y
servicios bajo demanda de retransmisión en directo («live-streaming») y vídeo.
Es propietaria de sus centros de proceso de datos y de todos los elementos críticos
para la prestación de los servicios y productos que suministra
(tanto software como hardware). 
</p>

<h2>Acerca de Google</h2>

<p>
<a href="https://google.com/">Google</a> es una de las mayores empresas tecnológicas del
mundo y proporciona un amplio rango de servicios y productos relacionados con Internet tales
como tecnologías de publicidad en línea, búsqueda, computación en la nube, software y hardware.
</p>

<p>
Google ha dado soporte a Debian patrocinando la DebConf más de
diez años y es también socio de Debian patrocinando partes
de la infraestructura de integración continua de <a href="https://salsa.debian.org">Salsa</a>
en la plataforma de la nube de Google («Google Cloud Platform»).
</p>

<h2>Acerca de Lenovo</h2>

<p>
Como líder tecnológico global en la fabricación de un amplio catálogo de productos conectados
incluyendo teléfonos inteligentes, tabletas, PC y estaciones de trabajo, dispositivos de realidad aumentada y de realidad virtual,
soluciones de hogar, oficina y centros de datos inteligentes, <a href="https://www.lenovo.com">Lenovo</a>
entiende lo críticos que son los sistemas y plataformas abiertos para un mundo conectado.
</p>

<h2>Información de contacto</h2>

<p>Para más información, visite la página web de DebConf19 en
<a href="https://debconf19.debconf.org/">https://debconf19.debconf.org/</a>
o envíe un correo electrónico a &lt;press@debian.org&gt;.</p>
